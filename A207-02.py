import pylightbulbclient
import pyaudioclient

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "27-Scene-of-the-Crime-1.wav")

dmx.set_chan_range_fade(4, 18, 64, 7000)

audio.wait(12.8)
#Greek Chorus
dmx.set_chan_range_fade(1, 6, 255, 3000)

audio.wait(33)
dmx.set_chan_range(1, 18, 255)

print("Song Finished")
