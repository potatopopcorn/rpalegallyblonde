import pylightbulbclient
import pyaudioclient
import time

def method_name():
	fixt["LED1"].set_rgb(128, 16, 0, fade=1800)
	fixt["LED2"].set_rgb(128, 16, 0, fade=1800)
	fixt["LED3"].set_rgb(128, 16, 0, fade=1800)
	fixt["LED4"].set_rgb(128, 16, 0, fade=1800)
	fixt["LED5"].set_rgb(128, 16, 0, fade=1800)
	fixt["LED6"].set_rgb(128, 16, 0, fade=1800)
	time.sleep(1.9)
	fixt["LED1"].set_rgb(255, 0, 64)
	fixt["LED2"].set_rgb(255, 0, 64)
	fixt["LED3"].set_rgb(255, 0, 64)
	fixt["LED4"].set_rgb(255, 0, 64)
	fixt["LED5"].set_rgb(255, 0, 64)
	fixt["LED6"].set_rgb(255, 0, 64)

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "22-Bend-and-Snap.wav")


dmx.set_chan_range_fade(19, 200, 0, 3000)
dmx.set_chan_range_fade(2, 18, 192, 3000)

fixt["LED1"].set_rgb(64, 0, 4)
fixt["LED2"].set_rgb(64, 0, 4)
fixt["LED3"].set_rgb(64, 0, 4)
fixt["LED4"].set_rgb(64, 0, 4)
fixt["LED5"].set_rgb(64, 0, 4)
fixt["LED6"].set_rgb(64, 0, 4)

audio.wait(115)
#Boys
fixt["LED1"].set_rgb(0, 64, 64)
fixt["LED2"].set_rgb(0, 64, 64)
fixt["LED3"].set_rgb(0, 64, 64)
fixt["LED4"].set_rgb(0, 64, 64)
fixt["LED5"].set_rgb(0, 64, 64)
fixt["LED6"].set_rgb(0, 64, 64)

audio.wait(141)
# Final Change
fixt["LED1"].set_rgb(0, 64, 0)
fixt["LED2"].set_rgb(0, 64, 0)
fixt["LED3"].set_rgb(0, 64, 0)
fixt["LED4"].set_rgb(0, 64, 0)
fixt["LED5"].set_rgb(0, 64, 0)
fixt["LED6"].set_rgb(0, 64, 0)

audio.wait(180)
#Kyle
fixt["LED1"].set_rgb(0, 0, 0)
fixt["LED2"].set_rgb(0, 0, 0)
fixt["LED3"].set_rgb(0, 0, 0)
fixt["LED4"].set_rgb(0, 0, 0)
fixt["LED5"].set_rgb(0, 0, 0)
fixt["LED6"].set_rgb(0, 0, 0)

audio.wait(195)

dmx.blackout()

print("Song Finished")
