from pylightbulbclient import LightClient, Fixture, LoadLighting

LLoad = LoadLighting();
dmx = LLoad.get_dmx();
fixt = LLoad.load_fixtures();

dmx.blackout()

dmx.set_chan_range(1,18, 192)
fixt["LED1"].set_rgb(64,0,16)
fixt["LED2"].set_rgb(64,0,16)
fixt["LED3"].set_rgb(64,0,16)
fixt["LED4"].set_rgb(64,0,16)
fixt["LED5"].set_rgb(64,0,16)
fixt["LED6"].set_rgb(64,0,16)