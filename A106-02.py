import pylightbulbclient
import pyaudioclient

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "10-Positive.wav")

#Angelic Lights

fixt["Par6"].set_intensity(0, fade=3000)
fixt["Par11"].set_intensity(0, fade=3000)
fixt["Par12"].set_intensity(0, fade=3000)
fixt["Par17"].set_intensity(0, fade=3000)
fixt["Par18"].set_intensity(0, fade=3000)

fixt["Cyc3"].set_rgb(0,0,255, fade=3000)

fixt["LED1"].set_rgb(64, 16, 48) # So tempting to use 191, you're lucky I'm feeling nice
fixt["LED2"].set_rgb(64, 16, 48) # ^^ You are amazing! Don't even numbers look so much nicer?!
fixt["LED3"].set_rgb(64, 16, 48)
fixt["LED4"].set_rgb(64, 16, 48)
fixt["LED5"].set_rgb(64, 16, 48)
fixt["LED6"].set_rgb(64, 16, 48)

audio.wait(36)
#"Kill Her"

dmx.set_chan_range(2,4,192) # Change to 160 or 128, if too bright
dmx.set_chan_range(7,10,192)
dmx.set_chan_range(13,16,96)
fixt["Par5"].set_intensity(255)
fixt["LED1"].set_rgb(128, 0, 4)
fixt["LED2"].set_rgb(128, 0, 4)
fixt["LED3"].set_rgb(128, 0, 4)
fixt["LED4"].set_rgb(128, 0, 4)
fixt["LED5"].set_rgb(128, 0, 4)
fixt["LED6"].set_rgb(128, 0, 4)

audio.wait(86) #Probably a bit early, but thought that would be better tune if you want

#Move to stairs
fixt["Cyc5"].set_rgb(0,0,255, fade=1000)
#fixt["Rave4"].set_intensity(255)
#fixt["Rave4"].set_rgb(0,0,255, fade=1000)
#fixt["Rave2"].set_intensity(255)
#fixt["Rave2"].set_rgb(0,0,255, fade=1000)
fixt["Cyc3"].set_rgb(0,0,0)

audio.wait(130)
#Blues off, wash darker
dmx.set_chan_range(1,4,128)
dmx.set_chan_range(7,10,128)
dmx.set_chan_range(13,16,128)
fixt["Cyc3"].set_rgb(0,0,0)
fixt["Cyc5"].set_rgb(0,0,0)
fixt["Rave4"].set_rgb(0,0,0)

audio.wait(161)
#Pinks out, Brunette
fixt["LED1"].set_rgb(0, 0, 0)
fixt["LED2"].set_rgb(0, 0, 0)
fixt["LED3"].set_rgb(0, 0, 0)
fixt["LED4"].set_rgb(0, 0, 0)
fixt["LED5"].set_rgb(0, 0, 0)
fixt["LED6"].set_rgb(0, 0, 0)

audio.wait(186)

dmx.blackout()

print("Song Finished")
