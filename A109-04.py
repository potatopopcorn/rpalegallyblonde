import pylightbulbclient
import pyaudioclient

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "17-So-Much-Better.wav")


dmx.set_chan_range_fade(1,18, 64, 5000)
fixt["Cyc1"].set_rgb(0,0,255, fade=5000)
fixt["Cyc2"].set_rgb(0,0,255, fade=5000)
fixt["Cyc3"].set_rgb(20,0,255, fade=5000)
fixt["Cyc4"].set_rgb(0,0,255, fade=5000)
fixt["Cyc5"].set_rgb(0,0,255, fade=5000)
fixt["Cyc6"].set_rgb(0,0,255, fade=5000)
fixt["LED1"].set_rgb(0,0,64, fade=5000)
fixt["LED2"].set_rgb(0,0,64, fade=5000)
fixt["LED3"].set_rgb(0,0,64, fade=5000)
fixt["LED4"].set_rgb(0,0,64, fade=5000)
fixt["LED5"].set_rgb(0,0,64, fade=5000)
fixt["LED6"].set_rgb(0,0,64, fade=5000)

audio.wait(41)
dmx.set_chan_range(1,18, 192)
fixt["Cyc1"].set_rgb(255,128,0)
fixt["Cyc2"].set_rgb(255,128,0)
fixt["Cyc3"].set_rgb(255,128,0)
fixt["Cyc4"].set_rgb(255,128,0)
fixt["Cyc5"].set_rgb(255,128,0)
fixt["Cyc6"].set_rgb(255,128,0)
fixt["LED1"].set_rgb(128,64,0)
fixt["LED2"].set_rgb(128,64,0)
fixt["LED3"].set_rgb(128,64,0)
fixt["LED4"].set_rgb(128,64,0)
fixt["LED5"].set_rgb(128,64,0)
fixt["LED6"].set_rgb(128,64,0)

audio.wait(123.5)
dmx.set_chan_range_fade(1,18, 64, 6500)
fixt["Cyc1"].set_rgb(255,0,64, fade=6500)
fixt["Cyc2"].set_rgb(255,0,64, fade=6500)
fixt["Cyc3"].set_rgb(255,0,64, fade=6500)
fixt["Cyc4"].set_rgb(255,0,64, fade=6500)
fixt["Cyc5"].set_rgb(255,0,64, fade=6500)
fixt["Cyc6"].set_rgb(255,0,64, fade=6500)
fixt["LED1"].set_rgb(128,0,32, fade=6500)
fixt["LED2"].set_rgb(128,0,32, fade=6500)
fixt["LED3"].set_rgb(128,0,32, fade=6500)
fixt["LED4"].set_rgb(128,0,32, fade=6500)
fixt["LED5"].set_rgb(128,0,32, fade=6500)
fixt["LED6"].set_rgb(128,0,32, fade=6500)

audio.wait(140)
dmx.set_chan_range_fade(1,18, 192, 4000)

audio.wait(215)

print("Song Finished")
