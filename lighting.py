import socket

def connectToServer(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))

def disconnect():
    s.close()

def sendCommand(cmd):
    sendByteArray = bytearray(cmd, "ascii")
    s.send(len(cmd).to_bytes(4, byteorder='big') + sendByteArray)
    print(s.recv(1024))
