from pylightbulbclient import LightClient, Fixture, LoadLighting

LLoad = LoadLighting();
dmx = LLoad.get_dmx();
fixt = LLoad.load_fixtures();

dmx.blackout()

fixt["Par1"].set_intensity(128)
fixt["Par2"].set_intensity(128)
fixt["Par3"].set_intensity(128)
fixt["Par7"].set_intensity(128)
fixt["Par13"].set_intensity(96)
fixt["Rave1"].set_intensity(255)
fixt["Rave1"].set_rgb(0,0,255)