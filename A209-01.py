import pylightbulbclient
import pyaudioclient

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

dmx.blackout()
audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "30-Bows.wav")


dmx.blackout()

dmx.set_chan_range(1, 18, 255)

audio.wait(117) #reduce bo time

dmx.set_chan_range_fade(1, 200, 0, 5000)

print("Song Finished")
