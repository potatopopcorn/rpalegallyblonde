import pylightbulbclient
import pyaudioclient

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()


audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "06-What You Want - Part 2.wav")

audio.wait(4)

dmx.set_chan_range(1, 18, 255)
fixt["Cyc1"].set_rgb(0,0,0)
fixt["Cyc2"].set_rgb(0,0,0)
fixt["Cyc3"].set_rgb(0,0,0)
fixt["LED1"].set_rgb(255,52,42)
fixt["LED2"].set_rgb(255,52,42)
fixt["LED3"].set_rgb(255,52,42)
fixt["LED4"].set_rgb(255,52,42)
fixt["LED5"].set_rgb(255,52,42)
fixt["LED6"].set_rgb(255,52,42)

audio.wait(90)
fixt["LED1"].set_rgb(0,0,0)
fixt["LED2"].set_rgb(0,0,0)
fixt["LED3"].set_rgb(0,0,0)
fixt["LED4"].set_rgb(0,0,0)
fixt["LED5"].set_rgb(0,0,0)
fixt["LED6"].set_rgb(0,0,0)

audio.wait(100)
dmx.set_chan_range_fade(1, 18, 128, 5000)
fixt["LED1"].set_rgb(255,0,128, fade=20000)
fixt["LED2"].set_rgb(255,0,128, fade=20000)
fixt["LED3"].set_rgb(255,0,128, fade=20000)
fixt["LED4"].set_rgb(255,0,128, fade=20000)
fixt["LED5"].set_rgb(255,0,128, fade=20000)
fixt["LED6"].set_rgb(255,0,128, fade=20000)

audio.wait(150)
dmx.set_chan_range(1, 18, 255)
fixt["LED1"].set_rgb(255,52,42)
fixt["LED2"].set_rgb(255,52,42)
fixt["LED3"].set_rgb(255,52,42)
fixt["LED4"].set_rgb(255,52,42)
fixt["LED5"].set_rgb(255,52,42)
fixt["LED6"].set_rgb(255,52,42)

audio.wait(172)

dmx.blackout()

print("Song Finished")
