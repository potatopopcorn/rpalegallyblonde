from pylightbulbclient import LightClient, Fixture, LoadLighting

LLoad = LoadLighting();
dmx = LLoad.get_dmx();
fixt = LLoad.load_fixtures();

fixt["Cyc1"].set_rgb(255, 0, 0)
fixt["Cyc2"].set_rgb(255, 0, 0)
fixt["Cyc3"].set_rgb(255, 0, 0)
fixt["Cyc4"].set_rgb(255, 0, 0)
fixt["Cyc5"].set_rgb(255, 0, 0)
fixt["Cyc6"].set_rgb(255, 0, 0)
fixt["LED1"].set_rgb(64, 0, 0)
fixt["LED2"].set_rgb(64, 0, 0)
fixt["LED3"].set_rgb(64, 0, 0)
fixt["LED4"].set_rgb(64, 0, 0)
fixt["LED5"].set_rgb(64, 0, 0)
fixt["LED6"].set_rgb(64, 0, 0)
