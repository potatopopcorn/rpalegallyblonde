from pylightbulbclient import LightClient, Fixture, LoadLighting

LLoad = LoadLighting();
dmx = LLoad.get_dmx();
fixt = LLoad.load_fixtures();

fixt["Par1"].set_intensity(96, fade = 1000)
fixt["Par2"].set_intensity(0, fade = 1000)
fixt["Par3"].set_intensity(0, fade = 1000)
fixt["Par7"].set_intensity(0, fade = 1000)
fixt["Par7"].set_intensity(0, fade = 1000)
fixt["Rave1"].set_intensity(255)
fixt["Rave1"].set_rgb(0,0,255)