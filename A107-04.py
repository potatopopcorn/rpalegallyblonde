import pylightbulbclient
import pyaudioclient

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "12-Ireland-Reprise.wav")

audio.wait(23)
dmx.set_chan_range_fade(1, 18, 0, 1000)
fixt["Cyc1"].set_rgb(0,255,0, fade=1000)
fixt["Cyc2"].set_rgb(255,255,255, fade=1000)
fixt["Cyc3"].set_rgb(255,32,0, fade=1000)
fixt["Cyc4"].set_rgb(0,255,0, fade=1000)
fixt["Cyc5"].set_rgb(255,255,255, fade=1000)
fixt["Cyc6"].set_rgb(255,32,0, fade=1000)
fixt["LED1"].set_rgb(0,255,0, fade=1000)
fixt["LED2"].set_rgb(0,255,0, fade=1000)
fixt["LED3"].set_rgb(128,128,128, fade=1000)
fixt["LED4"].set_rgb(128,128,128, fade=1000)
fixt["LED5"].set_rgb(255,32,0, fade=1000)
fixt["LED6"].set_rgb(255,32,0, fade=1000) 

audio.wait(73)
dmx.blackout()

print("Song Finished")
