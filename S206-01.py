from pylightbulbclient import LightClient, Fixture, LoadLighting

LLoad = LoadLighting();
dmx = LLoad.get_dmx();
fixt = LLoad.load_fixtures();

fixt["Par5"].set_intensity(192)
fixt["Par6"].set_intensity(192)
fixt["Par11"].set_intensity(192)
fixt["Par12"].set_intensity(192)
fixt["Par17"].set_intensity(96)
fixt["LED4"].set_rgb(0, 64, 0)
fixt["LED5"].set_rgb(64, 64, 64)
fixt["LED6"].set_rgb(64, 8, 0)
