import pylightbulbclient
import pyaudioclient

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "20-Kyle_s-Theme-Walking.wav")

fixt["Par2"].set_intensity(192, fade = 2000)
fixt["Par3"].set_intensity(192, fade = 2000)
fixt["Par4"].set_intensity(192, fade = 2000)
fixt["Par7"].set_intensity(192, fade = 2000)
fixt["Par13"].set_intensity(192, fade = 2000)

audio.wait(16.7)

print("Song Finished")