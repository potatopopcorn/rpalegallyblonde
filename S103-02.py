from pylightbulbclient import LightClient, Fixture, LoadLighting

LLoad = LoadLighting();
dmx = LLoad.get_dmx();
fixt = LLoad.load_fixtures();

dmx.blackout()

fixt["Cyc1"].set_rgb(0,0,0)
fixt["Cyc2"].set_rgb(0,0,0)
fixt["Cyc3"].set_rgb(0,0,0)
fixt["Cyc4"].set_rgb(0,0,0)
fixt["Cyc5"].set_rgb(0,0,0)
fixt["Cyc6"].set_rgb(0,0,0)
fixt["Par9"].set_intensity(128)
fixt["Par10"].set_intensity(128)
fixt["Par15"].set_intensity(192)
fixt["Par16"].set_intensity(192)