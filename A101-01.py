import pylightbulbclient
import pyaudioclient
import time

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

dmx.blackout()
audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "01-02-Overture-Ohmigod-You-Guys.wav")

pos = audio.timecode()



dmx.blackout()

# Overture

audio.wait(45)
# Spotted 3 Verses
fixt["Par16"].set_intensity(64)
fixt["Par15"].set_intensity(64)
fixt["Par10"].set_intensity(64)
fixt["Par9"].set_intensity(64)
audio.wait(89)
# Chorus
dmx.set_chan_range(1, 18, 192)
audio.wait(118)
# Fall In Line Bit
dmx.set_chan_range_fade(1, 18, 128, 1500)
fixt["Cyc1"].set_rgb(128, 0, 32)
fixt["Cyc2"].set_rgb(128, 0, 32)
fixt["Cyc3"].set_rgb(128, 0, 32)
audio.wait(136)
# Quiet Bit
dmx.set_chan_range(1, 18, 64)
fixt["Par16"].set_intensity(0)
fixt["Par15"].set_intensity(0)
fixt["Par10"].set_intensity(0)
fixt["Par9"].set_intensity(128)
fixt["Cyc1"].set_rgb(0, 0, 128)
fixt["Cyc2"].set_rgb(0, 0, 128)
fixt["Cyc3"].set_rgb(0, 0, 128)
audio.wait(150)
# Before Delta Nu Bit
dmx.set_chan_range(1, 18, 192)
fixt["Cyc1"].set_rgb(128, 0, 32)
fixt["Cyc2"].set_rgb(128, 0, 32)
fixt["Cyc3"].set_rgb(128, 0, 32)
audio.wait(160)
# Delta Nu Bit
dmx.set_chan_range_fade(1, 18, 0, 500)
fixt["Par3"].set_intensity(96, fade=500)
fixt["Par4"].set_intensity(96, fade=500)
fixt["Par10"].set_intensity(96, fade=500)
fixt["Cyc1"].set_rgb(128, 0, 128, fade=500)
fixt["Cyc2"].set_rgb(128, 0, 128, fade=500)
fixt["Cyc3"].set_rgb(128, 0, 128, fade=500)
audio.wait(188)
# Bit Before Dog
dmx.set_chan_range_fade(1, 18, 192, 5000)
audio.wait(193)
# Dog
fixt["Cyc1"].set_rgb(0, 0, 0)
fixt["Cyc2"].set_rgb(0, 0, 0)
fixt["Cyc3"].set_rgb(0, 0, 0)
audio.wait(209)
fixt["Cyc1"].set_rgb(128, 0, 64, fade=3000)
fixt["Cyc2"].set_rgb(128, 0, 64, fade=3000)
fixt["Cyc3"].set_rgb(128, 0, 64, fade=3000)
audio.wait(227)
dmx.set_chan_range_fade(1, 18, 0, 10000)
fixt["Cyc1"].set_rgb(128, 0, 64, fade=10000)
fixt["Cyc2"].set_rgb(128, 0, 64, fade=10000)
fixt["Cyc3"].set_rgb(128, 0, 64, fade=10000)
audio.wait(239)
# Elle Enters
fixt["Cyc1"].set_rgb(0, 0, 0)
fixt["Cyc3"].set_rgb(0, 0, 0)
fixt["Cyc2"].set_rgb(0, 0, 255)
fixt["Cyc5"].set_rgb(0, 0, 255)
audio.wait(263)
# Seeing Elle
fixt["Cyc2"].set_rgb(0, 0, 0)
fixt["Cyc5"].set_rgb(0, 0, 0)
dmx.set_chan_range(1, 18, 192)
audio.wait(288)
dmx.set_chan_range(1, 18, 128)
fixt["Cyc1"].set_rgb(128, 255, 255)
fixt["Cyc2"].set_rgb(128, 255, 255)
fixt["Cyc3"].set_rgb(128, 255, 255)
fixt["Cyc4"].set_rgb(128, 255, 255)
fixt["Cyc5"].set_rgb(128, 255, 255)
fixt["Cyc6"].set_rgb(128, 255, 255)
audio.wait(355)
dmx.set_chan_range(1, 18, 128)
fixt["Cyc1"].set_rgb(255, 0, 255)
fixt["Cyc2"].set_rgb(255, 0, 255)
fixt["Cyc3"].set_rgb(255, 0, 255)
fixt["Cyc4"].set_rgb(255, 0, 255)
fixt["Cyc5"].set_rgb(255, 0, 255)
fixt["Cyc6"].set_rgb(255, 0, 255)
fixt["LED1"].set_rgb(255, 0, 255, fade=5000)
fixt["LED2"].set_rgb(255, 0, 255, fade=5000)
fixt["LED3"].set_rgb(255, 0, 255, fade=5000)
fixt["LED4"].set_rgb(255, 0, 255, fade=5000)
fixt["LED5"].set_rgb(255, 0, 255, fade=5000)
fixt["LED6"].set_rgb(255, 0, 255, fade=5000)
audio.wait(394.5)
# Flash effect - may want to edit this
dmx.set_chan_range(1, 18, 0)
fixt["Cyc1"].set_rgb(255, 255, 255)
fixt["Cyc2"].set_rgb(255, 255, 255)
fixt["Cyc3"].set_rgb(255, 255, 255)
fixt["Cyc4"].set_rgb(255, 255, 255)
fixt["Cyc5"].set_rgb(255, 255, 255)
fixt["Cyc6"].set_rgb(255, 255, 255)
fixt["LED1"].set_rgb(255, 255, 255)
fixt["LED2"].set_rgb(255, 255, 255)
fixt["LED3"].set_rgb(255, 255, 255)
fixt["LED4"].set_rgb(255, 255, 255)
fixt["LED5"].set_rgb(255, 255, 255)
fixt["LED6"].set_rgb(255, 255, 255)
time.sleep(0.7)
dmx.blackout()


print("Song Finished")
