import pylightbulbclient
import pyaudioclient

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

dmx.blackout()
audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "19-Take-it-Like-a-Man.wav")

dmx.set_chan_range_fade(2, 18, 128, 3000)

fixt["Cyc1"].set_rgb(0, 64, 64, fade=3000)
fixt["Cyc1"].set_rgb(128, 255, 255, fade=3000)
fixt["Cyc2"].set_rgb(128, 255, 255, fade=3000)
fixt["Cyc3"].set_rgb(128, 255, 255, fade=3000)
fixt["Cyc4"].set_rgb(128, 255, 255, fade=3000)
fixt["Cyc5"].set_rgb(128, 255, 255, fade=3000)
fixt["Cyc6"].set_rgb(128, 255, 255, fade=3000)
fixt["LED1"].set_rgb(0, 64, 64, fade=3000)
fixt["LED2"].set_rgb(0, 64, 64, fade=3000)
fixt["LED3"].set_rgb(0, 64, 64, fade=3000)
fixt["LED4"].set_rgb(0, 64, 64, fade=3000)
fixt["LED5"].set_rgb(0, 64, 64, fade=3000)
fixt["LED6"].set_rgb(0, 64, 64, fade=3000)

audio.wait(160.5)
fixt["LED1"].set_rgb(64, 48, 0)
fixt["LED2"].set_rgb(64, 48, 0)
fixt["LED3"].set_rgb(64, 48, 0)
fixt["LED4"].set_rgb(64, 48, 0)
fixt["LED5"].set_rgb(64, 48, 0)
fixt["LED6"].set_rgb(64, 48, 0)

audio.wait(248.5) # Make earlier
dmx.blackout()

print("Song Finished")
