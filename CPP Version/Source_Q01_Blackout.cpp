#include "show.h"


int cue()
{
  lbClient->sendCommand("Blackout()");
  return 0;
}

int main(int argc, char ** argv)
{
  if(!startShow(argc, argv))
  {
    //SHOW FAIL
    return -1;
  }

  int ret = cue();

  endShow();

  return ret;
}
