#pragma once 

#include "client.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <bitset>
#include <map>
#include <utility>

using namespace boost;

class Fixture
{
public:
    Fixture(std::string filename, unsigned char address, LightBulbClient* LBclient);
    ~Fixture();
    void setIntensity(int value, unsigned short fade = 0); // set by DMX value
	void setIntensity(float percentage, unsigned short fade = 0); // set by percentage
	void setRGB(int r, int g, int b, unsigned short fade = 0); // set colour with RGB channels
	void setPan(int value, unsigned short fade = 0); // set by DMX value
	void setPan(float angle, unsigned short fade = 0); // set by angle
	void setTilt(int value, unsigned short fade = 0); // set by DMX value
	void setTilt(float angle, unsigned short fade = 0); // set by angle
	void setFunction(std::string chan, int value, unsigned short fade = 0); // set channel by name and DMX value
	void setFunction(std::string chan, float percentage, unsigned short fade = 0); // set channel by name and percentage
	void setMacro(std::string chan, std::string macro); // set channel by macro name
private:
    LightBulbClient* client;
    unsigned char dmxAddr; // starting address
	unsigned char numChannels; // number of channels fixture uses
	std::map<std::string, int> chanList; // map of channel name and DMX address for channels used by fixture
	property_tree::ptree pt; // contains data from JSON
};