#ifndef SHOW_H
#define SHOW_H

#include <cstdio>
#include <ctime>
#include <exception>
#include <set>
#include <string>

#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/thread/thread.hpp>

#include "audioclient.h"
#include "client.h"
#include "fixtures.h"

const std::string CONFPATH = "conf.json";

extern std::string lighting_fixtureAddress;

//Show Start And Stop
bool startShow();
void stopShow();

//Logging
enum ErrorLevel{FATAL = 1,CRITICAL = 2,ERROR = 4,WARNING = 8,ALERT = 16,
  INFO = 32,STATUS = 64,SYSVERBOSE = 128};
void log(std::string msg, ErrorLevel level);

//Wrappers
void wait(int msec);

//Audio
void playSong(std::string fileName);
void stopSong();
double getTimeCode();

//Lighing
extern LightBulbClient *lbClient;

//Projections

#endif //SHOW_H
