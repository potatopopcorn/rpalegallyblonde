#include "client.h"

LightBulbClient::LightBulbClient(std::string ipAddress, unsigned short port) // default port 21000
    : m_socket(m_io_context)
{
    m_socket.connect(tcp::endpoint(boost::asio::ip::address::from_string(ipAddress), port));
}

LightBulbClient::~LightBulbClient()
{
    m_socket.close();
}

void LightBulbClient::setChan(unsigned short chan, unsigned short val)
{
    if(chan < 0 || chan > 512)
    {
        std::cerr << "Invalid Channel Specified" << std::endl;
        return;
    }
    std::string outputString = "SetChan(";
    outputString.append(std::to_string(chan));
    outputString.append(", ");
    outputString.append(std::to_string(val));
    outputString.append(")");
    sendBytes(outputString.c_str(), outputString.length());
}

void LightBulbClient::setChanFade(unsigned short chan, unsigned short val, unsigned short time)
{
    if(chan < 0 || chan > 512)
    {
        std::cerr << "Invalid Channel Specified" << std::endl;
        return;
    }
    std::string outputString = "SetChanFade(";
    outputString.append(std::to_string(chan));
    outputString.append(", ");
    outputString.append(std::to_string(val));
    outputString.append(", ");
    outputString.append(std::to_string(time));
    outputString.append(")");
    sendBytes(outputString.c_str(), outputString.length());
}

void LightBulbClient::sendBytes(const char bytes[], int numBytes)
{
    int i = 0;
    boost::system::error_code err;

    unsigned char message[numBytes + 4];

    message[0] = (numBytes >> 24) & 0xFF;
    message[1] = (numBytes >> 16) & 0xFF;
    message[2] = (numBytes >> 8) & 0xFF;
    message[3] = numBytes & 0xFF;

    for(int j = 0; j < numBytes; j++)
    {
      message[j+4] = bytes[j];
    }
    numBytes += 4;
    try
    {
        while(!err && numBytes > i)
        {
            i += m_socket.write_some(boost::asio::buffer(message + i, numBytes - i), err);
        }
        if(err)
        {
            throw boost::system::system_error(err);
        }
        while(true)
        {
          boost::array<char, 128> buf;
          boost::system::error_code error;

          size_t len = m_socket.read_some(boost::asio::buffer(buf), error);

          if (error == boost::asio::error::eof)
          {
            break; // Connection closed cleanly by peer.
          }
          else if (error)
          {
            throw boost::system::system_error(error); // Some other error.
          }
          std::cout.write(buf.data(), len);
          if(len >= 9){
            break;
          }
        }

    }
    catch(std::exception& e)
    {
        std::cerr << "Error Sending Message: " << e.what() << std::endl;
    }
}

void LightBulbClient::sendCommand(std::string command)
{
  sendBytes(command.c_str(), command.length());
}
