#ifndef CULTURALCENTRE_H
#define CULTURALCENTRE_H
#include "show.h"

// 24 Pars
// Moving heads, 19 each 2x
// LED 6 each 6x
// Cyc 3 each 6x
// Par64 6 each 4x

extern Fixture *Par1;
extern Fixture Par2;
extern Fixture Par3;
extern Fixture Par4;
extern Fixture Par5;
extern Fixture Par6;
extern Fixture Par7;
extern Fixture Par8;
extern Fixture Par9;
extern Fixture Par10;
extern Fixture Par11;
extern Fixture Par12;
extern Fixture Par13;
extern Fixture Par14;
extern Fixture Par15;
extern Fixture Par16;
extern Fixture Par17;
extern Fixture Par18;
extern Fixture Par19;
extern Fixture Par20;
extern Fixture Par21;
extern Fixture Par22;
extern Fixture Par23;
extern Fixture Par24;

extern Fixture MH1;
extern Fixture MH2;

extern Fixture *LED1;
extern Fixture LED2;
extern Fixture LED3;
extern Fixture LED4;
extern Fixture LED5;
extern Fixture LED6;

extern Fixture Cyc1;
extern Fixture Cyc2;
extern Fixture Cyc3;
extern Fixture Cyc4;
extern Fixture Cyc5;
extern Fixture Cyc6;

extern Fixture Rave1;
extern Fixture Rave2;
extern Fixture Rave3;
extern Fixture Rave4;

void loadFixtures(std::string path, LightBulbClient *client);
#endif // CULTURALCENTRE_H