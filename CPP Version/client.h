#pragma once

#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <string>
#include <iostream>

#include <cstdio>

using namespace boost::asio;
using ip::tcp;

class LightBulbClient
{
public:
    LightBulbClient(std::string ipAddress, unsigned short port = 5005);
    ~LightBulbClient();
    void setChan(unsigned short chan, unsigned short val);
    void setChanFade(unsigned short chan, unsigned short val, unsigned short time);
    void sendCommand(std::string command);
private:
    boost::asio::io_context m_io_context;
    tcp::socket m_socket;
    boost::system::error_code m_boostErr;

    void sendBytes(const char bytes[], int numBytes);
};
