#include "show.h"

std::string audio_pathPrefix;
std::string audio_serverAddress;
std::string lighting_serverAddress;
std::string lighting_fixtureAddress;
unsigned short lighting_serverPort;

LightBulbClient *lbClient;


Client *audioC;

bool startShow()
{
  log("Show Started", SYSVERBOSE);
  boost::property_tree::ptree tree;
  boost::property_tree::read_json(CONFPATH, tree);
  audio_pathPrefix = tree.get<std::string>("audio.pathprefix");
  audio_serverAddress = tree.get<std::string>("audio.serveraddress");
  log("Audio Path Prefix: " + audio_pathPrefix, SYSVERBOSE);
  log("Audio Server Address: " + audio_serverAddress, SYSVERBOSE);
  audioC = new Client(audio_serverAddress);
  lighting_serverAddress = tree.get<std::string>("lighting.serveraddress");
  lighting_fixtureAddress = tree.get<std::string>("lighting.fixturepath");

  lbClient = new LightBulbClient(lighting_serverAddress, 21000);

  log("Show Started Successfully", SYSVERBOSE);
  return true;
}

void stopShow()
{
  delete lbClient;
  delete audioC;
  log("Show Finished", SYSVERBOSE);
}
void log(std::string msg, ErrorLevel level)
{
  /*
  LEVELS:
  1: FATAL - Error that could result in damage
  2: CRITICAL - Error that is not recoverable,
  4: ERROR - Error that is fairly localised, Bad but not scary
  8: WARNING - Not great, but can be delt with
  16: ALERT - Good to know, could lead to something bigger, but not really an isse
  32: INFO - Important status messages
  64: STATUS - What the system is up to
  128: SYS VERBOSE - What the system is doing behind the scenes
  */
  std::string levelName = "";
  switch(level){
    case 1:
      levelName = "FATL";
      break;
    case 2:
      levelName = "CRIT";
      break;
    case 4:
      levelName = "ERRR";
      break;
    case 8:
      levelName = "WARN";
      break;
    case 16:
      levelName = "ALRT";
      break;
    case 32:
      levelName = "INFO";
      break;
    case 64:
      levelName = "STAT";
      break;
    case 128:
      levelName = "SYSV";
      break;
  }

  time_t curr_time;
	curr_time = time(NULL);
	tm *tm_gmt = gmtime(&curr_time);

  printf("%s [%02i:%02i:%02i]: %s\n",
  levelName.c_str(), tm_gmt->tm_hour, tm_gmt->tm_min, tm_gmt->tm_sec,
  msg.c_str());
}

void wait(int msec)
{
  boost::this_thread::sleep(boost::posix_time::milliseconds(msec));
}

void playSong(std::string fileName)
{
  std::string fullPath = audio_pathPrefix;
  fullPath += fileName;
  log("Starting Song: " + fullPath, SYSVERBOSE);
  audioC->play(fullPath);
}

void stopSong()
{
  audioC->stop();
}

double getTimeCode()
{
  return audioC->timecode();
}
