#include "fixtures.h"

Fixture::Fixture(std::string filename, unsigned char address, LightBulbClient *LBclient)
	: dmxAddr(address), numChannels(0), client(LBclient)
{
	try
	{
		printf("Loading Fixture %i\n", dmxAddr);
		property_tree::read_json(filename, pt); // parse file
		if ((dmxAddr < 0) | (dmxAddr > 511))
		{
			perror("Address out of range\n");
			return;
		}
		numChannels = pt.get<int>("channels");
		printf("Staring for loop\n");
		for (property_tree::ptree::value_type &channel : pt.get_child("availableChannels")) // fill chanList
		{
			printf("Running for loop\n");
			std::string name = channel.first; // get name of channel
			printf("LN1\n");
			int value = pt.get<int>((format("availableChannels.%1%.offset") % name).str()); // get offset
			printf("LN2\n");
			chanList.insert(std::pair<std::string, int>(name, value));
			printf("LN3\n");
		}
		printf("Finished Loading Fixture %i\n", dmxAddr);
	}
	catch (...)
	{
		perror("File not supported");
	}
}

Fixture::~Fixture()
{

}

void Fixture::setIntensity(int value, unsigned short fade) // default fade = 0
{
	try
	{
		if(fade = 0)
        {
            client->setChan(dmxAddr + chanList.at("intensity"), value);
        }
        else
        {
            client->setChanFade(dmxAddr + chanList.at("intensity"), value, fade);
        }
	}
	catch(std::out_of_range)
	{
		perror("Fixture does not have intensity capability\n");
	}
}

void Fixture::setIntensity(float percentage, unsigned short fade) // default fade = 0
{
	int value = (percentage / 100) * 255; // convert from percentage to DMX value
	setIntensity(value, fade);
}

void Fixture::setRGB(int r, int g, int b, unsigned short fade) // default fade = 0
{
	try
	{
        if(fade = 0)
        {
            client->setChan(dmxAddr + chanList.at("red"), r);
            client->setChan(dmxAddr + chanList.at("green"), g);
            client->setChan(dmxAddr + chanList.at("blue"), b);
        }
        else
        {
            client->setChanFade(dmxAddr + chanList.at("red"), r, fade);
            client->setChanFade(dmxAddr + chanList.at("green"), g, fade);
            client->setChanFade(dmxAddr + chanList.at("blue"), b, fade);
        }
		
	}
	catch (std::out_of_range)
	{
		perror("Fixture does not have RGB capability\n");
	}
}

void Fixture::setPan(int value, unsigned short fade) // default fade = 0
{
	try
	{
        if(fade = 0)
        {
            client->setChan(dmxAddr + chanList.at("pan"), value);
        }
        else
        {
            client->setChanFade(dmxAddr + chanList.at("pan"), value, fade);
        }
		
	}
	catch (std::out_of_range)
	{
		perror("Fixture does not have pan capability\n");
	}
}

void Fixture::setPan(float angle, unsigned short fade) // default fade = 0
{
	try
	{
		float dmx_per_deg = 255 / pt.get<int>("availableChannels.pan.range");
		int value = angle * dmx_per_deg; // convert from angle to degree
        setPan(value, fade);
	}
	catch (property_tree::ptree_error)
	{
		perror("Fixture does not have pan capability\n");
	}
}

void Fixture::setTilt(int value, unsigned short fade) // default fade = 0
{
	try
	{
        if(fade = 0)
        {
            client->setChan(dmxAddr + chanList.at("tilt"), value);
        }
		else
        {
            client->setChanFade(dmxAddr + chanList.at("tilt"), value, fade);
        }
	}
	catch (std::out_of_range)
	{
		perror("Fixture does not have tilt capability\n");
	}
}

void Fixture::setTilt(float angle, unsigned short fade) // default fade = 0
{
	try
	{
		float dmx_per_deg = 255 / pt.get<int>("availableChannels.tilt.range");
		int value = angle * dmx_per_deg;
        setTilt(value, fade);
	}
	catch (property_tree::ptree_error)
	{
		perror("Fixture does not have tilt capability\n");
	}
}

void Fixture::setFunction(std::string chan, int value, unsigned short fade) // default fade = 0
{
	try
	{
        if(fade = 0)
        {
            client->setChan(dmxAddr + chanList.at(chan), value);
        }
		else
        {
            client->setChanFade(dmxAddr + chanList.at(chan), value, fade);
        }
	}
	catch (std::out_of_range)
	{
		perror("Fixture does not have capability\n");
	}
}

void Fixture::setFunction(std::string chan, float percentage, unsigned short fade) // default fade = o
{
	int value = (percentage / 100) * 255; // convert from percentage to DMX value
	setFunction(chan, value, fade);
}

void Fixture::setMacro(std::string chan, std::string macro)
{
	try
	{
		int value = pt.get<int>((format("availableChannels.%1%.capabilities.%2%.startVal") % chan % macro).str()); // find DMX value of macro
		client->setChan(dmxAddr + chanList.at(chan), value);
	}
	catch (property_tree::ptree_error)
	{
		perror("Fixture does not have capability\n");
	}
}