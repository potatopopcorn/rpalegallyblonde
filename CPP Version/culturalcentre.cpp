#include "culturalcentre.h"

Fixture *Par1;
Fixture *LED1;

void loadFixtures(std::string path, LightBulbClient *client)
{
    Par1 = new Fixture(path+"Dimmer.json", 1, client);
    Fixture Par2(path+"Dimmer.json", 2, client);
    Fixture Par3(path+"Dimmer.json", 3, client);
    Fixture Par4(path+"Dimmer.json", 4, client);
    Fixture Par5(path+"Dimmer.json", 5, client);
    Fixture Par6(path+"Dimmer.json", 6, client);
    Fixture Par7(path+"Dimmer.json", 7, client);
    Fixture Par8(path+"Dimmer.json", 8, client);
    Fixture Par9(path+"Dimmer.json", 9, client);
    Fixture Par10(path+"Dimmer.json", 10, client);
    Fixture Par11(path+"Dimmer.json", 11, client);
    Fixture Par12(path+"Dimmer.json", 12, client);
    Fixture Par13(path+"Dimmer.json", 13, client);
    Fixture Par14(path+"Dimmer.json", 14, client);
    Fixture Par15(path+"Dimmer.json", 15, client);
    Fixture Par16(path+"Dimmer.json", 16, client);
    Fixture Par17(path+"Dimmer.json", 17, client);
    Fixture Par18(path+"Dimmer.json", 18, client);
    Fixture Par19(path+"Dimmer.json", 19, client);
    Fixture Par20(path+"Dimmer.json", 20, client);
    Fixture Par21(path+"Dimmer.json", 21, client);
    Fixture Par22(path+"Dimmer.json", 22, client);
    Fixture Par23(path+"Dimmer.json", 23, client);
    Fixture Par24(path+"Dimmer.json", 24, client);

    // Fixture MH1(25);
    // Fixture MH2(44);

    Fixture Cyc1(path+"RGB.json", 102, client);
    Fixture Cyc2(path+"RGB.json", 105, client);
    Fixture Cyc3(path+"RGB.json", 108, client);
    Fixture Cyc4(path+"RGB.json", 111, client);
    Fixture Cyc5(path+"RGB.json", 114, client);
    Fixture Cyc6(path+"RGB.json", 117, client);

    Fixture Rave1(path+"Led_Par_64.json", 120, client);
    Fixture Rave2(path+"Led_Par_64.json", 126, client);
    Fixture Rave3(path+"Led_Par_64.json", 132, client);
    Fixture Rave4(path+"Led_Par_64.json", 138, client);

    LED1 = new Fixture(path+"RGBWAU_Pro_Par.json", 144, client);
    Fixture LED2(path+"RGBWAU_Pro_Par.json", 150, client);
    Fixture LED3(path+"RGBWAU_Pro_Par.json", 156, client);
    Fixture LED4(path+"RGBWAU_Pro_Par.json", 162, client);
    Fixture LED5(path+"RGBWAU_Pro_Par.json", 168, client);
    Fixture LED6(path+"RGBWAU_Pro_Par.json", 174, client);

    
}