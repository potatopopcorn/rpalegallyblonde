#include "show.h"


int cue()
{
  lbClient->sendCommand("Blackout()");
  lbClient->sendCommand("SetChanRang(1, 24, 255)");
  return 0;
}

int main(int argc, char ** argv)
{
  if(!startShow(argc, argv))
  {
    //SHOW FAIL
    return -1;
  }

  int ret = cue();

  endShow();

  return ret;
}
