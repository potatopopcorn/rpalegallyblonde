#include "lbshow.h"

int cue()
{
  playSong("27-Scene-of-the-Crime-1.wav");
  wait(100);
  double time = 0;
  while(time < 190)
  {
    time = getTimeCode();
    printf("%f\n", time);
    wait(100);
  }
  stopSong();
}

int main(int argc, char ** argv)
{
  if(!loadShow(argc, argv))
  {
    //SHOW FAIL
    return -1;
  }

  int ret = cue();

  endShow();

  return 0;
}
