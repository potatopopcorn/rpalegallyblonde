#include "show.h"

int cue()
{
  playSong("11-Ireland.wav");
  wait(100);
  double time = 0;
  while(time < 190)
  {
    time = getTimeCode();
    printf("%f\n", time);
    lbClient->setChan(1, (int) time);
    wait(100);
  }
  stopSong();
}

int main(int argc, char ** argv)
{
  if(!startShow(argc, argv))
  {
    //SHOW FAIL
    return -1;
  }

  int ret = cue();

  endShow();

  return 0;
}
