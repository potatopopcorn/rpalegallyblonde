from pylightbulbclient import LightClient, Fixture, LoadLighting

LLoad = LoadLighting();
dmx = LLoad.get_dmx();
fixt = LLoad.load_fixtures();

dmx.set_chan_range_fade(2, 12, 192, 3000)
fixt["LED1"].set_rgb(0, 64, 0, fade=3000)
fixt["LED2"].set_rgb(64, 64, 64, fade=3000)
fixt["LED3"].set_rgb(64, 8, 0, fade=3000)
