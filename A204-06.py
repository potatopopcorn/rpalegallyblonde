import pylightbulbclient
import pyaudioclient
import time

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

audio = pyaudioclient.Client(pyaudioclient.audio_ip)

def method_name():
	fixt["LED1"].set_rgb(128, 16, 0, fade=1800)
	fixt["LED2"].set_rgb(128, 16, 0, fade=1800)
	fixt["LED3"].set_rgb(128, 16, 0, fade=1800)
	fixt["LED4"].set_rgb(128, 16, 0, fade=1800)
	fixt["LED5"].set_rgb(128, 16, 0, fade=1800)
	fixt["LED6"].set_rgb(128, 16, 0, fade=1800)
	time.sleep(1.9)
	fixt["LED1"].set_rgb(255, 128, 0)
	fixt["LED2"].set_rgb(255, 128, 0)
	fixt["LED3"].set_rgb(255, 128, 0)
	fixt["LED4"].set_rgb(255, 128, 0)
	fixt["LED5"].set_rgb(255, 128, 0)
	fixt["LED6"].set_rgb(255, 128, 0)

audio.play(pyaudioclient.audio_dir + "23-Bend-and-Snap-Sound-Effect.wav")

method_name()
dmx.set_chan_range(15, 16, 128)
dmx.set_chan_range(9, 12, 192)
dmx.set_chan_range(3, 6, 192)

audio.wait(4)

fixt["LED1"].set_rgb(0, 0, 0, fade=1000)
fixt["LED2"].set_rgb(0, 0, 0, fade=1000)
fixt["LED3"].set_rgb(0, 0, 0, fade=1000)
fixt["LED4"].set_rgb(0, 0, 0, fade=1000)
fixt["LED5"].set_rgb(0, 0, 0, fade=1000)
fixt["LED6"].set_rgb(0, 0, 0, fade=1000)

print("Song Finished")
