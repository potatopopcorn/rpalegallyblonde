import socket
import errno
import time
import json
from PIL import ImageColor
import os

fixtures_path =  os.path.join(os.path.dirname(__file__), 'Fixtures/')

class LightClient:
	def __init__(self, host, port = 21000):
		self.failover_ip = "192.168.2.80" #Change this
		self.failover_enable = False

		self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		for attempt in range(20):
			try:
				self.s.connect((host, port))
			except EnvironmentError as exc:
				if exc.errno == errno.ECONNREFUSED:
					time.sleep(0.2)
			else:
				break

		if self.failover_enable:
			self.fo_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			for attempt in range(20):
				try:
					self.fo_sock.connect((self.failover_ip, port))
				except EnvironmentError as exc:
					if exc.errno == errno.ECONNREFUSED:
						time.sleep(0.2)
				else:
					break

	def __del__(self):
		self.s.close()
		if self.failover_enable:
			self.fo_sock.close()

	def send_to_server(self, cmd):
		try:
			send_byte_array = bytearray(cmd, "ascii")
			self.s.send(len(cmd).to_bytes(4, byteorder='big') + send_byte_array)
			recv_msg = self.s.recv(1024)
			print(recv_msg)
		except IOError as err:
			errno, strerror = err.args
			print("IOError ({0}): {1}".format(errno, strerror))

		if self.failover_enable:
			try:
				fo_byte_array = bytearray(cmd, "ascii")
				self.fo_sock.send(len(cmd).to_bytes(4, byteorder='big') + fo_byte_array)
				fo_msg = self.fo_sock.recv(1024)
				print("Failover: " + fo_msg)
			except IOError as err:
				errno, strerror = err.args
				print("IOError ({0}): {1}".format(errno, strerror))

		# time.sleep(0.002)

	def set_chan(self, chan, val):
		if not 1 <= chan <= 512:
			raise ValueError("Invalid channel specified: %s" % str(chan))
		val = max(0, min(val, 255))
		cmd = "SetChan(" + str(chan) + ", " + str(val) + ")"
		self.send_to_server(cmd)

	def set_chan_fade(self, chan, val, time):
		if not 1 <= chan <= 512:
			raise ValueError("Invalid channel specified: %s" % str(chan))
		val = max(0, min(val, 255))
		cmd = "SetChanFade(" + str(chan) + ", " + str(val) + ", " + str(time) + ")"
		self.send_to_server(cmd)

	def blackout(self):
		cmd = "Blackout()"
		self.send_to_server(cmd)

	def set_all_chans(self, val):
		val = max(0, min(val, 255))
		cmd = "SetAllChans(" + str(val) + ")"
		self.send_to_server(cmd)

	def set_all_chans_fade(self, val, time):
		val = max(0, min(val, 255))
		cmd = "SetAllChansFade(" + str(val) + "," + str(time) + ")"
		self.send_to_server(cmd)

	def set_chan_range(self, start_chan, end_chan, val):
		if not 1 <= start_chan <= 512 or not 1 <= end_chan <= 512:
			 raise ValueError("Invalid channel specified: %s and %s" % str(start_chan), str(end_chan))
		val = max(0, min(val, 255))
		cmd = "SetChanRang(" + str(start_chan) + "," + str(end_chan) + "," + str(val) + ")"
		self.send_to_server(cmd)

	def set_chan_range_fade(self, start_chan, end_chan, val, time):
		if not 1 <= start_chan <= 512 or not 1 <= end_chan <= 512:
			 raise ValueError("Invalid channel specified: %s and %s" % str(start_chan), str(end_chan))
		val = max(0, min(val, 255))
		cmd = "SetChanRangFade(" + str(start_chan) + "," + str(end_chan) + "," + str(val) + "," + str(time) + ")"
		self.send_to_server(cmd)

	def set_chan_range_interval(self, start_chan, end_chan, intv, val):
		if not 1 <= start_chan <= 512 or not 1 <= end_chan <= 512:
			 raise ValueError("Invalid channel specified: %s and %s" % str(start_chan), str(end_chan))
		val = max(0, min(val, 255))
		cmd = "SetChanRangIntv(" + str(start_chan) + "," + str(end_chan) + "," + str(intv) + "," + str(val) + ")"
		self.send_to_server(cmd)

	def set_chan_range_interval_fade(self, start_chan, end_chan, intv, val, time):
		if not 1 <= start_chan <= 512 or not 1 <= end_chan <= 512:
			 raise ValueError("Invalid channel specified: %s and %s" % str(start_chan), str(end_chan))
		val = max(0, min(val, 255))
		cmd = "SetChanRangIntv(" + str(start_chan) + "," + str(end_chan) + "," + str(intv) + "," + str(val) + str(fade) + ")"
		self.send_to_server(cmd)


class Fixture():
	def __init__(self, connection, path, address):
		with open(path, 'r') as f:
			self.data = json.load(f)
		self.address = address
		self.link = connection
		self.name = self.data["shortName"]
		self.speed_offset = {}
		self.macro_offset = {}

	def __str__(self):
		return "{}, Address = {}".format(self.name, self.address)

	def set_rgb(self, r, g, b, fade = 0):
		r_offset = self.data["availableChannels"]["red"]["offset"]
		g_offset = self.data["availableChannels"]["green"]["offset"]
		b_offset = self.data["availableChannels"]["blue"]["offset"]
		if fade == 0:
			self.link.set_chan(self.address + r_offset, r)
			self.link.set_chan(self.address + g_offset, g)
			self.link.set_chan(self.address + b_offset, b)
		else:
			self.link.set_chan_fade(self.address + r_offset, r, fade)
			self.link.set_chan_fade(self.address + g_offset, g, fade)
			self.link.set_chan_fade(self.address + b_offset, b, fade)

	def set_colour(self, colour, fade = 0):
		r, g, b = ImageColor.getcolor(colour, "RGB")
		self.set_rgb(r, g, b, fade)

	def set_intensity(self, val, fade = 0):
		intensity_offset = self.data["availableChannels"]["intensity"]["offset"]
		if isinstance(val, float):
			val = int((val/100) * 255)
		if fade == 0:
			self.link.set_chan(self.address + intensity_offset, val)
		else:
			self.link.set_chan_fade(self.address + intensity_offset, val, fade)

	def set_pan(self, val, fade = 0):
		pan_offset = self.data["availableChannels"]["pan"]["offset"]
		if isinstance(val, float):
			val = int((val/100) * 255)
		if fade == 0:
			self.link.set_chan(self.address + pan_offset, val)
		else:
			self.link.set_chan_fade(self.address + pan_offset, val, fade)

	def set_tilt(self, val, fade = 0):
		tilt_offset = self.data["availableChannels"]["tilt"]["offset"]
		if isinstance(val, float):
			val = int((val/100) * 255)
		if fade == 0:
			self.link.set_chan(self.address + tilt_offset, val)
		else:
			self.link.set_chan_fade(self.address + tilt_offset, val, fade)

	def set_function(self, name, val, fade = 0):
		function_offset = self.data["availableChannels"][name]["offset"]
		if isinstance(val, float):
			val = int((val/100) * 255)
		if fade == 0:
			self.link.set_chan(self.address + function_offset, val)
		else:
			self.link.set_chan_fade(self.address + function_offset, val, fade)

	def set_macro(self, name, label):
		macro_offset = self.data["availableChannels"][name]["offset"]
		val = self.data["availableChannels"][name]["capabilities"][label]["startVal"]
		self.link.set_chan(self.address + macro_offset, val)

	def list_channels(self):
		return list(self.data["availableChannels"].keys())

class LoadLighting():
	def __init__(self):
		self.dmx = LightClient("127.0.0.1")

	def get_dmx(self):
		return self.dmx

	def load_fixtures(self):
		Fix = dict()
		Fix["Par1"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 1)
		Fix["Par2"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 2)
		Fix["Par3"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 3)
		Fix["Par4"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 4)
		Fix["Par5"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 5)
		Fix["Par6"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 6)
		Fix["Par7"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 7)
		Fix["Par8"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 8)
		Fix["Par9"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 9)
		Fix["Par10"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 10)
		Fix["Par11"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 11)
		Fix["Par12"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 12)
		Fix["Par13"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 13)
		Fix["Par14"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 14)
		Fix["Par15"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 15)
		Fix["Par16"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 16)
		Fix["Par17"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 17)
		Fix["Par18"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 18)
		Fix["Par19"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 19)
		Fix["Par20"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 20)
		Fix["Par21"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 21)
		Fix["Par22"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 22)
		Fix["Par23"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 23)
		Fix["Par24"] = Fixture(self.dmx, fixtures_path + "Dimmer.json", 24)

		#Fix["MH1"] = Fixture(self.dmx, fixtures_path + "XA-400.json", 25)
		#Fix["MH2"] = Fixture(self.dmx, fixtures_path + "XA-400.json", 47)

		Fix["Cyc1"] = Fixture(self.dmx, fixtures_path + "RGB.json", 102)
		Fix["Cyc2"] = Fixture(self.dmx, fixtures_path + "RGB.json", 105)
		Fix["Cyc3"] = Fixture(self.dmx, fixtures_path + "RGB.json", 108)
		Fix["Cyc4"] = Fixture(self.dmx, fixtures_path + "RGB.json", 111)
		Fix["Cyc5"] = Fixture(self.dmx, fixtures_path + "RGB.json", 114)
		Fix["Cyc6"] = Fixture(self.dmx, fixtures_path + "RGB.json", 117)

		Fix["Rave1"] = Fixture(self.dmx, fixtures_path + "Led_Par_64.json", 120)
		Fix["Rave2"] = Fixture(self.dmx, fixtures_path + "Led_Par_64.json", 126)
		Fix["Rave3"] = Fixture(self.dmx, fixtures_path + "Led_Par_64.json", 132)
		Fix["Rave4"] = Fixture(self.dmx, fixtures_path + "Led_Par_64.json", 138)

		Fix["LED1"] = Fixture(self.dmx, fixtures_path + "RGBWAU_Pro_Par.json", 144)
		Fix["LED2"] = Fixture(self.dmx, fixtures_path + "RGBWAU_Pro_Par.json", 150)
		Fix["LED3"] = Fixture(self.dmx, fixtures_path + "RGBWAU_Pro_Par.json", 156)
		Fix["LED4"] = Fixture(self.dmx, fixtures_path + "RGBWAU_Pro_Par.json", 162)
		Fix["LED5"] = Fixture(self.dmx, fixtures_path + "RGBWAU_Pro_Par.json", 168)
		Fix["LED6"] = Fixture(self.dmx, fixtures_path + "RGBWAU_Pro_Par.json", 174)

		return Fix
