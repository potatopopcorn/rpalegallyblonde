from pylightbulbclient import LightClient, Fixture, LoadLighting

LLoad = LoadLighting();
dmx = LLoad.get_dmx();
fixt = LLoad.load_fixtures();

dmx.blackout()

dmx.set_chan_range(1, 18, 255)
fixt["Cyc1"].set_rgb(0,255,255)
fixt["Cyc2"].set_rgb(0,255,255)
fixt["Cyc3"].set_rgb(0,255,255)
fixt["Cyc4"].set_rgb(0,255,255)
fixt["Cyc5"].set_rgb(0,255,255)
fixt["Cyc6"].set_rgb(0,255,255)