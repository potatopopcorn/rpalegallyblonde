from pylightbulbclient import LightClient, Fixture, LoadLighting

LLoad = LoadLighting();
dmx = LLoad.get_dmx();
fixt = LLoad.load_fixtures();

fixt["Par3"].set_intensity(191)
fixt["Par4"].set_intensity(191)
fixt["Par5"].set_intensity(191)
fixt["Par6"].set_intensity(191)
fixt["Par9"].set_intensity(191)
fixt["Par10"].set_intensity(191)
fixt["Par11"].set_intensity(191)
fixt["Par12"].set_intensity(191)
fixt["Par18"].set_intensity(191)

fixt["Cyc3"].set_rgb(0,0,0, fade=1000)
fixt["Cyc6"].set_rgb(0,0,0, fade=1000)
fixt["LED4"].set_rgb(0,0,0, fade=1000)
fixt["LED5"].set_rgb(0,0,0, fade=1000)
fixt["LED6"].set_rgb(0,0,0, fade=1000)