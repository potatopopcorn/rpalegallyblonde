import pylightbulbclient
import pyaudioclient

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

dmx.blackout()
audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "07-The Harvard Variations - Part 1.wav")


dmx.blackout()

dmx.set_chan_range(1, 18, 128)
fixt["Par1"].set_intensity(191)
fixt["Par2"].set_intensity(191)
fixt["Cyc1"].set_rgb(255,32,0)
fixt["Rave1"].set_intensity(255)
fixt["Rave1"].set_rgb(255,32,0)

audio.wait(73)
dmx.set_chan_range(1, 18, 255)

print("Song Finished")
