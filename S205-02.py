from pylightbulbclient import LightClient, Fixture, LoadLighting

LLoad = LoadLighting();
dmx = LLoad.get_dmx();
fixt = LLoad.load_fixtures();

fixt["Par3"].set_intensity(192, fade=5000)
fixt["Par4"].set_intensity(192, fade=5000)
fixt["Par8"].set_intensity(0, fade=5000)
fixt["Par11"].set_intensity(0, fade=5000)
fixt["Par14"].set_intensity(0, fade=5000)
fixt["Par15"].set_intensity(0, fade=5000)
fixt["Par14"].set_intensity(64, fade=5000)
fixt["Par17"].set_intensity(0, fade=5000)
