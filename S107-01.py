from pylightbulbclient import LightClient, Fixture, LoadLighting

LLoad = LoadLighting();
dmx = LLoad.get_dmx();
fixt = LLoad.load_fixtures();

dmx.blackout()


fixt["Par5"].set_intensity(191)
fixt["Par6"].set_intensity(191)
fixt["Par11"].set_intensity(191)
fixt["Par12"].set_intensity(191)
fixt["Par18"].set_intensity(191)
fixt["Cyc3"].set_rgb(255,255,0)
fixt["Cyc6"].set_rgb(255,255,0)