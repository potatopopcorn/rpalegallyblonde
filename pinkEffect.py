import random
import time
from pylightbulbclient import LightClient, Fixture, LoadLighting

LLoad = LoadLighting();
dmx = LLoad.get_dmx();
fixt = LLoad.load_fixtures();

dmx.blackout()

fixt["LED1"].set_rgb(255, 0, 64);
fixt["LED2"].set_rgb(255, 0, 64);
fixt["LED3"].set_rgb(255, 0, 64);
fixt["LED4"].set_rgb(255, 0, 64);
fixt["LED5"].set_rgb(255, 0, 64);
fixt["LED6"].set_rgb(255, 0, 64);

while True:
    fixt["LED1"].set_rgb(255, 0, random.randint(16, 128), 900);
    fixt["LED2"].set_rgb(255, 0, random.randint(16, 128), 900);
    fixt["LED3"].set_rgb(255, 0, random.randint(16, 128), 900);
    fixt["LED4"].set_rgb(255, 0, random.randint(16, 128), 900);
    fixt["LED5"].set_rgb(255, 0, random.randint(16, 128), 900);
    fixt["LED6"].set_rgb(255, 0, random.randint(16, 128), 900);

    time.sleep(1)
