import pylightbulbclient
import pyaudioclient
import time

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "13-Serious-(Reprise).wav")

dmx.set_chan_range_fade(1, 18, 0, 3000)

fixt["LED4"].set_rgb(0,0,0, fade=1000)
fixt["LED5"].set_rgb(0,0,0, fade=1000)
fixt["LED6"].set_rgb(0,0,0, fade=1000)
time.sleep(0.05)
fixt["LED1"].set_rgb(0,0,255, fade=1000)
fixt["LED2"].set_rgb(0,0,255, fade=1000)
fixt["LED3"].set_rgb(0,0,255, fade=1000)


audio.wait(49.3)
audio.stop()


print("Song Finished")

dmx.set_chan_range(1,12,192)
fixt["LED1"].set_rgb(64,48,0)
fixt["LED2"].set_rgb(64,48,0)
fixt["LED3"].set_rgb(64,48,0)
fixt["LED4"].set_rgb(64,48,0)
fixt["LED5"].set_rgb(64,48,0)
fixt["LED6"].set_rgb(64,48,0)