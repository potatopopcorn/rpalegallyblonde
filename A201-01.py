import pylightbulbclient
import pyaudioclient

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

dmx.blackout()
audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "18-Whipped-into-Shape.wav")

dmx.set_chan_range(1, 18, 255)
fixt["LED1"].set_rgb(0, 32, 64)
fixt["LED2"].set_rgb(0, 32, 64)
fixt["LED3"].set_rgb(0, 32, 64)
fixt["LED4"].set_rgb(0, 32, 64)
fixt["LED5"].set_rgb(0, 32, 64)
fixt["LED6"].set_rgb(0, 32, 64)

audio.wait(60)
# Law team enters
dmx.set_chan_range(1, 18, 128)
fixt["Par1"].set_intensity(255)
fixt["Par2"].set_intensity(255)
fixt["Cyc1"].set_rgb(0,0,255)
fixt["Cyc2"].set_rgb(0,0,255)
fixt["Cyc3"].set_rgb(0,0,255)
fixt["Cyc4"].set_rgb(0,0,255)
fixt["Cyc5"].set_rgb(0,0,255)
fixt["Cyc6"].set_rgb(0,0,255)
fixt["LED1"].set_rgb(0, 0, 0)
fixt["LED2"].set_rgb(0, 0, 0)
fixt["LED3"].set_rgb(0, 0, 0)
fixt["LED4"].set_rgb(0, 0, 0)
fixt["LED5"].set_rgb(0, 0, 0)
fixt["LED6"].set_rgb(0, 0, 0)

audio.wait(91)
# Brooke the second
fixt["Par9"].set_intensity(128)
fixt["Par10"].set_intensity(128)
fixt["Par15"].set_intensity(255)
fixt["Par16"].set_intensity(255)
fixt["Cyc1"].set_rgb(0,0,0)
fixt["Cyc2"].set_rgb(0,0,0)
fixt["Cyc3"].set_rgb(0,0,0)
fixt["Cyc4"].set_rgb(0,0,0)
fixt["Cyc5"].set_rgb(0,0,0)
fixt["Cyc6"].set_rgb(0,0,0)
fixt["LED1"].set_rgb(64, 8, 0)
fixt["LED2"].set_rgb(64, 8, 0)
fixt["LED3"].set_rgb(64, 8, 0)
fixt["LED4"].set_rgb(64, 8, 0)
fixt["LED5"].set_rgb(64, 8, 0)
fixt["LED6"].set_rgb(64, 8, 0)

audio.wait(108.8)
dmx.set_chan_range(2, 18, 192)

audio.wait(132.8)
# Brooke is guilty
dmx.set_chan_range(1, 18, 128)
fixt["Par1"].set_intensity(255)
fixt["Par2"].set_intensity(255)
fixt["Cyc1"].set_rgb(255,32,0)
fixt["Cyc2"].set_rgb(255,32,0)
fixt["Cyc3"].set_rgb(255,32,0)
fixt["Cyc4"].set_rgb(255,32,0)
fixt["Cyc5"].set_rgb(255,32,0)
fixt["Cyc6"].set_rgb(255,32,0)
fixt["LED1"].set_rgb(0, 0, 0)
fixt["LED2"].set_rgb(0, 0, 0)
fixt["LED3"].set_rgb(0, 0, 0)
fixt["LED4"].set_rgb(0, 0, 0)
fixt["LED5"].set_rgb(0, 0, 0)
fixt["LED6"].set_rgb(0, 0, 0)

audio.wait(173)
# Brooke is in jail
dmx.set_chan_range_fade(1, 18, 255, 3000)
fixt["LED1"].set_rgb(64, 8, 0, fade=3000)
fixt["LED2"].set_rgb(64, 8, 0, fade=3000)
fixt["LED3"].set_rgb(64, 8, 0, fade=3000)
fixt["LED4"].set_rgb(64, 8, 0, fade=3000)
fixt["LED5"].set_rgb(64, 8, 0, fade=3000)
fixt["LED6"].set_rgb(64, 8, 0, fade=3000)

audio.wait(265)
#Begin fade at end
dmx.set_chan_range_fade(11, 18, 0, 3000)
dmx.set_chan_range_fade(7, 8, 0, 3000)
fixt["Cyc1"].set_rgb(0, 0, 0, fade=3000)
fixt["Cyc2"].set_rgb(0, 0, 0, fade=3000)
fixt["Cyc3"].set_rgb(0, 0, 0, fade=3000)
fixt["Cyc4"].set_rgb(0, 0, 0, fade=3000)
fixt["Cyc5"].set_rgb(0, 0, 0, fade=3000)
fixt["Cyc6"].set_rgb(0, 0, 0, fade=3000)
fixt["LED1"].set_rgb(0, 0, 0, fade=3000)
fixt["LED2"].set_rgb(0, 0, 0, fade=3000)
fixt["LED3"].set_rgb(0, 0, 0, fade=3000)
fixt["LED4"].set_rgb(0, 0, 0, fade=3000)
fixt["LED5"].set_rgb(0, 0, 0, fade=3000)
fixt["LED6"].set_rgb(0, 0, 0, fade=3000)

audio.wait(273)



print("Song Finished")
