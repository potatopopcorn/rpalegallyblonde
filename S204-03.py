from pylightbulbclient import LightClient, Fixture, LoadLighting

LLoad = LoadLighting();
dmx = LLoad.get_dmx();
fixt = LLoad.load_fixtures();

dmx.set_chan_range_fade(2, 5, 64, 3000)
dmx.set_chan_range_fade(7, 18, 64, 3000)
