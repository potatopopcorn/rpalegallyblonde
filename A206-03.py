import pylightbulbclient
import pyaudioclient

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "26-Legally-Blonde-Remix.wav")

audio.wait(35.7)
dmx.set_chan_range_fade(4, 6, 0, 3000)
dmx.set_chan_range_fade(11, 18, 0, 3000)
fixt["LED1"].set_rgb(0,0,64, fade=3000)
fixt["LED2"].set_rgb(0,0,64, fade=3000)
fixt["LED3"].set_rgb(0,0,64, fade=3000)
fixt["LED4"].set_rgb(0,0,64, fade=3000)
fixt["LED5"].set_rgb(0,0,64, fade=3000)
fixt["LED6"].set_rgb(0,0,64, fade=3000)

audio.wait(79.5)
dmx.set_chan_range_fade(2, 18, 192, 3000)
fixt["LED1"].set_rgb(64,0,8)
fixt["LED2"].set_rgb(64,0,8)
fixt["LED3"].set_rgb(64,0,8)
fixt["LED4"].set_rgb(64,0,8)
fixt["LED5"].set_rgb(64,0,8)
fixt["LED6"].set_rgb(64,0,8)


audio.wait(221)
fixt["LED1"].set_rgb(0,0,0)
fixt["LED2"].set_rgb(0,0,0)
fixt["LED3"].set_rgb(0,0,0)
fixt["LED4"].set_rgb(0,0,0)
fixt["LED5"].set_rgb(0,0,0)
fixt["LED6"].set_rgb(0,0,0)
dmx.set_chan_range(4, 18, 128) # What are you trying to do here??
fixt["Par2"].set_intensity(255) # How tired were you when you did this?
fixt["Par3"].set_intensity(255)
fixt["Par7"].set_intensity(255)
fixt["Par8"].set_intensity(255)

print("Song Finished")
