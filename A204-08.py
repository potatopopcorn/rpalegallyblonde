import pylightbulbclient
import pyaudioclient
import time

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "24-There-Right-There.wav")

def static_rainbow(fade_time):
    fixt["LED1"].set_rgb(64, 0, 0, fade=fade_time*1000)
    fixt["LED2"].set_rgb(64, 64, 0, fade=fade_time*1000)
    fixt["LED3"].set_rgb(0, 64, 0, fade=fade_time*1000)
    fixt["LED4"].set_rgb(0, 64, 64, fade=fade_time*1000)
    fixt["LED5"].set_rgb(0, 0, 64, fade=fade_time*1000)
    fixt["LED6"].set_rgb(64, 0, 64, fade=fade_time*1000)

def fading_rainbow(fade_time, cut_time):
    while True:
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(64, 0, 0, fade=fade_time*1000-100)
        fixt["LED2"].set_rgb(64, 64, 0, fade=fade_time*1000-100)
        fixt["LED3"].set_rgb(0, 64, 0, fade=fade_time*1000-100)
        fixt["LED4"].set_rgb(0, 64, 64, fade=fade_time*1000-100)
        fixt["LED5"].set_rgb(0, 0, 64, fade=fade_time*1000-100)
        fixt["LED6"].set_rgb(64, 0, 64, fade=fade_time*1000-100)
        time.sleep(fade_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(64, 64, 0, fade=fade_time*1000-100)
        fixt["LED2"].set_rgb(0, 64, 0, fade=fade_time*1000-100)
        fixt["LED3"].set_rgb(0, 64, 64, fade=fade_time*1000-100)
        fixt["LED4"].set_rgb(0, 0, 64, fade=fade_time*1000-100)
        fixt["LED5"].set_rgb(64, 0, 64, fade=fade_time*1000-100)
        fixt["LED6"].set_rgb(64, 0, 0, fade=fade_time*1000-100)
        time.sleep(fade_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(0, 64, 0, fade=fade_time*1000-100)
        fixt["LED2"].set_rgb(0, 64, 64, fade=fade_time*1000-100)
        fixt["LED3"].set_rgb(0, 0, 64, fade=fade_time*1000-100)
        fixt["LED4"].set_rgb(64, 0, 64, fade=fade_time*1000-100)
        fixt["LED5"].set_rgb(64, 0, 0, fade=fade_time*1000-100)
        fixt["LED6"].set_rgb(64, 64, 0, fade=fade_time*1000-100)
        time.sleep(fade_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(0, 64, 64, fade=fade_time*1000-100)
        fixt["LED2"].set_rgb(0, 0, 64, fade=fade_time*1000-100)
        fixt["LED3"].set_rgb(64, 0, 64, fade=fade_time*1000-100)
        fixt["LED4"].set_rgb(64, 0, 0, fade=fade_time*1000-100)
        fixt["LED5"].set_rgb(64, 64, 0, fade=fade_time*1000-100)
        fixt["LED6"].set_rgb(0, 64, 0, fade=fade_time*1000-100)
        time.sleep(fade_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(0, 0, 64, fade=fade_time*1000-100)
        fixt["LED2"].set_rgb(64, 0, 64, fade=fade_time*1000-100)
        fixt["LED3"].set_rgb(64, 0, 0, fade=fade_time*1000-100)
        fixt["LED4"].set_rgb(64, 64, 0, fade=fade_time*1000-100)
        fixt["LED5"].set_rgb(0, 64, 0, fade=fade_time*1000-100)
        fixt["LED6"].set_rgb(0, 64, 64, fade=fade_time*1000-100)
        time.sleep(fade_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(64, 0, 64, fade=fade_time*1000-100)
        fixt["LED2"].set_rgb(64, 0, 0, fade=fade_time*1000-100)
        fixt["LED3"].set_rgb(64, 64, 0, fade=fade_time*1000-100)
        fixt["LED4"].set_rgb(0, 64, 0, fade=fade_time*1000-100)
        fixt["LED5"].set_rgb(0, 64, 64, fade=fade_time*1000-100)
        fixt["LED6"].set_rgb(0, 0, 64, fade=fade_time*1000-100)
        time.sleep(fade_time)

def discrete_rainbow(change_time, cut_time):
    while True:
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(64, 0, 0)
        fixt["LED2"].set_rgb(64, 64, 0)
        fixt["LED3"].set_rgb(0, 64, 0)
        fixt["LED4"].set_rgb(0, 64, 64)
        fixt["LED5"].set_rgb(0, 0, 64)
        fixt["LED6"].set_rgb(64, 0, 64)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(64, 64, 0)
        fixt["LED2"].set_rgb(0, 64, 0)
        fixt["LED3"].set_rgb(0, 64, 64)
        fixt["LED4"].set_rgb(0, 0, 64)
        fixt["LED5"].set_rgb(64, 0, 64)
        fixt["LED6"].set_rgb(64, 0, 0)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(0, 64, 0)
        fixt["LED2"].set_rgb(0, 64, 64)
        fixt["LED3"].set_rgb(0, 0, 64)
        fixt["LED4"].set_rgb(64, 0, 64)
        fixt["LED5"].set_rgb(64, 0, 0)
        fixt["LED6"].set_rgb(64, 64, 0)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(0, 64, 64)
        fixt["LED2"].set_rgb(0, 0, 64)
        fixt["LED3"].set_rgb(64, 0, 64)
        fixt["LED4"].set_rgb(64, 0, 0)
        fixt["LED5"].set_rgb(64, 64, 0)
        fixt["LED6"].set_rgb(0, 64, 0)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(0, 0, 64)
        fixt["LED2"].set_rgb(64, 0, 64)
        fixt["LED3"].set_rgb(64, 0, 0)
        fixt["LED4"].set_rgb(64, 64, 0)
        fixt["LED5"].set_rgb(0, 64, 0)
        fixt["LED6"].set_rgb(0, 64, 64)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(64, 0, 64)
        fixt["LED2"].set_rgb(64, 0, 0)
        fixt["LED3"].set_rgb(64, 64, 0)
        fixt["LED4"].set_rgb(0, 64, 0)
        fixt["LED5"].set_rgb(0, 64, 64)
        fixt["LED6"].set_rgb(0, 0, 64)
        time.sleep(change_time)

def static_europe(fade_time):
    fixt["LED1"].set_rgb(64, 48, 0, fade=fade_time*1000)
    fixt["LED2"].set_rgb(0, 0, 64, fade=fade_time*1000)
    fixt["LED3"].set_rgb(64, 48, 0, fade=fade_time*1000)
    fixt["LED4"].set_rgb(0, 0, 64, fade=fade_time*1000)
    fixt["LED5"].set_rgb(64, 48, 0, fade=fade_time*1000)
    fixt["LED6"].set_rgb(0, 0, 64, fade=fade_time*1000)

def discrete_europe(change_time, cut_time):
    while True:
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(64, 48, 0)
        fixt["LED2"].set_rgb(0, 0, 64)
        fixt["LED3"].set_rgb(64, 48, 0)
        fixt["LED4"].set_rgb(0, 0, 64)
        fixt["LED5"].set_rgb(64, 48, 0)
        fixt["LED6"].set_rgb(0, 0, 64)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(0, 0, 64)
        fixt["LED2"].set_rgb(64, 48, 0)
        fixt["LED3"].set_rgb(0, 0, 64)
        fixt["LED4"].set_rgb(64, 48, 0)
        fixt["LED5"].set_rgb(0, 0, 64)
        fixt["LED6"].set_rgb(64, 48, 0)
        time.sleep(change_time)

def discrete_mixed(change_time, cut_time):
    while True:
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(64, 0, 0)
        fixt["LED2"].set_rgb(64, 64, 0)
        fixt["LED3"].set_rgb(0, 64, 0)
        fixt["LED4"].set_rgb(0, 64, 64)
        fixt["LED5"].set_rgb(0, 0, 64)
        fixt["LED6"].set_rgb(64, 0, 64)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(64, 64, 0)
        fixt["LED2"].set_rgb(0, 64, 0)
        fixt["LED3"].set_rgb(0, 64, 64)
        fixt["LED4"].set_rgb(0, 0, 64)
        fixt["LED5"].set_rgb(64, 0, 64)
        fixt["LED6"].set_rgb(0, 0, 64)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(0, 64, 0)
        fixt["LED2"].set_rgb(0, 64, 64)
        fixt["LED3"].set_rgb(0, 0, 64)
        fixt["LED4"].set_rgb(64, 0, 64)
        fixt["LED5"].set_rgb(0, 0, 64)
        fixt["LED6"].set_rgb(64, 48, 0)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(0, 64, 64)
        fixt["LED2"].set_rgb(0, 0, 64)
        fixt["LED3"].set_rgb(64, 0, 64)
        fixt["LED4"].set_rgb(0, 0, 64)
        fixt["LED5"].set_rgb(64, 48, 0)
        fixt["LED6"].set_rgb(0, 0, 64)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(0, 0, 64)
        fixt["LED2"].set_rgb(64, 0, 64)
        fixt["LED3"].set_rgb(0, 0, 64)
        fixt["LED4"].set_rgb(64, 48, 0)
        fixt["LED5"].set_rgb(0, 0, 64)
        fixt["LED6"].set_rgb(64, 48, 0)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(64, 0, 64)
        fixt["LED2"].set_rgb(0, 0, 64)
        fixt["LED3"].set_rgb(64, 48, 0)
        fixt["LED4"].set_rgb(0, 0, 64)
        fixt["LED5"].set_rgb(64, 48, 0)
        fixt["LED6"].set_rgb(0, 0, 64)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(0, 0, 64)
        fixt["LED2"].set_rgb(64, 48, 0)
        fixt["LED3"].set_rgb(0, 0, 64)
        fixt["LED4"].set_rgb(64, 48, 0)
        fixt["LED5"].set_rgb(0, 0, 64)
        fixt["LED6"].set_rgb(64, 48, 0)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(64, 48, 0)
        fixt["LED2"].set_rgb(0, 0, 64)
        fixt["LED3"].set_rgb(64, 48, 0)
        fixt["LED4"].set_rgb(0, 0, 64)
        fixt["LED5"].set_rgb(64, 48, 0)
        fixt["LED6"].set_rgb(64, 0, 0)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(0, 0, 64)
        fixt["LED2"].set_rgb(64, 48, 0)
        fixt["LED3"].set_rgb(0, 0, 64)
        fixt["LED4"].set_rgb(64, 48, 0)
        fixt["LED5"].set_rgb(64, 0, 0)
        fixt["LED6"].set_rgb(64, 64, 0)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(64, 48, 0)
        fixt["LED2"].set_rgb(0, 0, 64)
        fixt["LED3"].set_rgb(64, 48, 0)
        fixt["LED4"].set_rgb(64, 0, 0)
        fixt["LED5"].set_rgb(64, 64, 0)
        fixt["LED6"].set_rgb(0, 64, 0)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(0, 0, 64)
        fixt["LED2"].set_rgb(64, 48, 0)
        fixt["LED3"].set_rgb(64, 0, 0)
        fixt["LED4"].set_rgb(64, 64, 0)
        fixt["LED5"].set_rgb(0, 64, 0)
        fixt["LED6"].set_rgb(0, 64, 64)
        time.sleep(change_time)
        if audio.timecode() > cut_time:
            break
        fixt["LED1"].set_rgb(64, 48, 0)
        fixt["LED2"].set_rgb(64, 0, 0)
        fixt["LED3"].set_rgb(64, 64, 0)
        fixt["LED4"].set_rgb(0, 64, 0)
        fixt["LED5"].set_rgb(0, 64, 64)
        fixt["LED6"].set_rgb(0, 0, 64)
        time.sleep(change_time)



dmx.set_chan_range_fade(2, 6, 96, 3000)
dmx.set_chan_range_fade(9, 18, 96, 3000)

audio.wait(2.5)
static_rainbow(4)
audio.wait(21)
fading_rainbow(0.5, 39.5)
static_europe(0)
audio.wait(57)
discrete_europe(0.5, 76)
fixt["LED1"].set_rgb(0, 0, 64, fade=3000)
fixt["LED2"].set_rgb(0, 0, 64, fade=3000)
fixt["LED3"].set_rgb(64, 64, 64, fade=3000)
fixt["LED4"].set_rgb(64, 64, 64, fade=3000)
fixt["LED5"].set_rgb(64, 0, 0, fade=3000)
fixt["LED6"].set_rgb(64, 0, 0, fade=3000)
discrete_europe(0.5, 92.3)
fading_rainbow(0.5, 106)
discrete_mixed(0.5, 140.5)
fixt["LED1"].set_rgb(0, 0, 0, fade=1000)
fixt["LED2"].set_rgb(0, 0, 0, fade=1000)
fixt["LED3"].set_rgb(0, 0, 0, fade=1000)
fixt["LED4"].set_rgb(0, 0, 0, fade=1000)
fixt["LED5"].set_rgb(0, 0, 0, fade=1000)
fixt["LED6"].set_rgb(0, 0, 0, fade=1000)
audio.wait(160.2)
static_rainbow(0)
fixt["LED1"].set_rgb(0, 0, 0, fade=1000)
fixt["LED2"].set_rgb(0, 0, 0, fade=1000)
fixt["LED3"].set_rgb(0, 0, 0, fade=1000)
fixt["LED4"].set_rgb(0, 0, 0, fade=1000)
fixt["LED5"].set_rgb(0, 0, 0, fade=1000)
fixt["LED6"].set_rgb(0, 0, 0, fade=1000)
audio.wait(166.6)
static_europe(0)
fixt["LED1"].set_rgb(0, 0, 0, fade=1000)
fixt["LED2"].set_rgb(0, 0, 0, fade=1000)
fixt["LED3"].set_rgb(0, 0, 0, fade=1000)
fixt["LED4"].set_rgb(0, 0, 0, fade=1000)
fixt["LED5"].set_rgb(0, 0, 0, fade=1000)
fixt["LED6"].set_rgb(0, 0, 0, fade=1000)
audio.wait(166.6)
discrete_rainbow(0.5, 199)
discrete_mixed(0.5, 214)


dmx.blackout()

print("Song Finished")
