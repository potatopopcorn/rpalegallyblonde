from os import listdir
from os.path import isfile, join

base_path = "Bases/"
base_files = [f for f in listdir(base_path) if isfile(join(base_path, f))]
