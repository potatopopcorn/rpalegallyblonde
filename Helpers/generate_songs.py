from os import listdir
from os.path import isfile, join
from shutil import copyfile
import wave
import contextlib
import math

base_file = "Bases/song.py"

song_path = "/home/yorka/Documents/LegallyBlonde/Music/"
song_files = [f for f in listdir(song_path) if isfile(join(song_path, f))]

for song in song_files:
    copyfile(base_file, "A"+song.split("-")[0]+".py")

    fname = song_path + song
    with contextlib.closing(wave.open(fname,'r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        duration = frames / float(rate)

    fileContents = open("A"+song.split("-")[0]+".py").read()
    fileContents = fileContents.replace("<<SONG>>", song)
    fileContents = fileContents.replace("<<TIME>>", str(math.ceil(duration)))
    fileWrite = open("A"+song.split("-")[0]+".py", 'w')
    fileWrite.write(fileContents)
    fileWrite.close()
