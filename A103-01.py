import pylightbulbclient
import pyaudioclient

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

dmx.blackout()
audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "04-Daughter-of-Delta-Nu.wav")

dmx.blackout()

dmx.set_chan_range(1, 18, 191)
fixt["Cyc1"].set_rgb(255,0,64)
fixt["Cyc2"].set_rgb(255,0,64)
fixt["Cyc3"].set_rgb(255,0,64)
fixt["Cyc4"].set_rgb(255,0,64)
fixt["Cyc5"].set_rgb(255,0,64)
fixt["Cyc6"].set_rgb(255,0,64)
audio.wait(3)
dmx.set_chan_range_fade(1, 18, 0, 3000)
fixt["Par3"].set_intensity(128)
fixt["Par4"].set_intensity(128)
fixt["Par9"].set_intensity(192)
fixt["Par10"].set_intensity(192)
fixt["Par11"].set_intensity(192)
fixt["Par11"].set_intensity(192)

audio.wait(34)

dmx.set_chan_range(1, 18, 255)

print("Song Finished")
