import pylightbulbclient
import pyaudioclient

LLoad = pylightbulbclient.LoadLighting()
dmx = LLoad.get_dmx()
fixt = LLoad.load_fixtures()

audio = pyaudioclient.Client(pyaudioclient.audio_ip)

audio.play(pyaudioclient.audio_dir + "25-Legally-Blonde.wav")

dmx.set_chan_range_fade(1, 18, 0, 3000)
fixt["Cyc1"].set_rgb(0, 0, 255, fade=15000)
fixt["Cyc2"].set_rgb(0, 0, 255, fade=15000)
fixt["Cyc3"].set_rgb(0, 0, 255, fade=15000)
fixt["Cyc4"].set_rgb(0, 0, 255, fade=15000)
fixt["Cyc5"].set_rgb(0, 0, 255, fade=15000)
fixt["Cyc6"].set_rgb(0, 0, 255, fade=15000)
fixt["LED1"].set_rgb(0, 0, 64, fade=15000)
fixt["LED2"].set_rgb(0, 0, 64, fade=15000)
fixt["LED3"].set_rgb(0, 0, 64, fade=15000)
fixt["LED4"].set_rgb(0, 0, 64, fade=15000)
fixt["LED5"].set_rgb(0, 0, 64, fade=15000)
fixt["LED6"].set_rgb(0, 0, 64, fade=15000)

audio.wait(116)
#Callahan hit on me
dmx.set_chan_range(1, 18, 128)

audio.wait(144)
#singing starts
dmx.set_chan_range_fade(1, 18, 0, 3000) # Sorry I am very confused here

audio.wait(201)
dmx.blackout()

print("Song Finished")
