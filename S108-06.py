from pylightbulbclient import LightClient, Fixture, LoadLighting

LLoad = LoadLighting();
dmx = LLoad.get_dmx();
fixt = LLoad.load_fixtures();

dmx.set_chan_range_fade(1, 2, 0, 1000)
dmx.set_chan_range_fade(3, 4, 192, 1000)
dmx.set_chan_range_fade(5, 8, 0, 1000)
dmx.set_chan_range_fade(9, 10, 192, 1000)
dmx.set_chan_range_fade(11, 14, 0, 1000)
dmx.set_chan_range_fade(15, 17, 192, 1000)
dmx.set_chan_range_fade(18, 0, 1000)

